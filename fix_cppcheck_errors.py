import sys, getopt, os
import xml.etree.ElementTree as ET

from cppcheck_fixer_core import CppCheckError
import cppcheck_fixer_utils

def print_usage():
    print('fix_cppcheck_errors.py -x <CppCheck scan XML file> -v <path to vip checkout>')


def main(argv):
   cppcheck_xml_file = ''
   vip_dir = ''
   try:
      opts, args = getopt.getopt(argv,"hx:v:",["cpp_file=","vip_dir="])
   except getopt.GetoptError:
      print("Error parsing arguments")
      print_usage()
      sys.exit(2)
   
   for opt, arg in opts:
      if opt == '-h':
         print_usage()
         sys.exit()
      elif opt in ("-x", "--cpp_file"):
         cppcheck_xml_file = arg
      elif opt in ("-v", "--vip_dir"):
         vip_dir = arg
      else:
          print("Unrecognized option: ", opt)

   if not cppcheck_xml_file or not vip_dir:
       print_usage()
       sys.exit()

   vip_dir = os.path.abspath(vip_dir)
   print('Cppcheck XML file is ', cppcheck_xml_file)
   print('vip checkout is ', vip_dir)

   imos_dir = os.path.join(vip_dir, 'imos')

   current_error = CppCheckError(CppCheckError.NONE)
   tree = ET.parse(cppcheck_xml_file)
   for elem in tree.iter():
       #print(elem)
       if elem.tag == "error":
           if elem.attrib["id"] == "postfixOperator":
               current_error = CppCheckError.POSTFIX_OPERATOR
           elif elem.attrib["id"] == "missingOverride":
               current_error = CppCheckError.MISSING_OVERRIDE
           else:
               current_error = CppCheckError.NONE
       elif elem.tag == "location":
           rel_file_path = elem.attrib['file']
           line_number = elem.attrib['line']
           full_path = os.path.join(imos_dir, rel_file_path)
           
           if current_error == CppCheckError.POSTFIX_OPERATOR:
               cppcheck_fixer_utils.fixCppCheckError(full_path, int(line_number), current_error)
           elif current_error == CppCheckError.MISSING_OVERRIDE:
               # Check this is the derived class where the override should go
               if elem.attrib['info'] == "Function in derived class":
                   cppcheck_fixer_utils.fixCppCheckError(full_path, int(line_number), current_error)
       elif elem.tag == "results" or elem.tag == "cppcheck" or elem.tag == "errors":
           continue
       else:
           print("Unrecognized element tag: ", elem.tag)

if __name__ == "__main__":
   main(sys.argv[1:])
