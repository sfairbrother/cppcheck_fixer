import re, os
import sys

from cppcheck_fixer_core import CppCheckError


# Returns prefixed version of string with postfix ops
# e.g.     for (_dateHelper dtHelper = from; dtHelper <= to; dtHelper++) 
# becomes  for (_dateHelper dtHelper = from; dtHelper <= to; ++dtHelper)
def getPrefixFromPostfixLine(line):
    words = re.split(', |; |\n |\t |\s', line)
    postfix_ops = [word for word in words if "++" in word or "--" in word]
    prefixed_line = line
    for post_op in postfix_ops:
        op = '+'
        if '+' in post_op: 
            op = '+'
        elif '-' in post_op:
            op = '-'
        else:
            return line

        no_op = post_op.replace(op, '')

        prefix_op = op + op + no_op
        prefixed_line = prefixed_line.replace(post_op, prefix_op)

    return prefixed_line


# Returns line with override specifier added
# e.g.     for (_dateHelper dtHelper = from; dtHelper <= to; dtHelper++) 
# becomes  for (_dateHelper dtHelper = from; dtHelper <= to; ++dtHelper)
def getOverrideLine(line):
    new_line = ''

    # Flag for whether we've applied override 
    b_need_to_add_override = True
    # Override will always be after function parentheses
    b_reached_closed_paren = False
    for c in line:

        if b_reached_closed_paren and b_need_to_add_override and (c == ';' or c == '{' or c == '='):
            # Add extra padding after override if it's being added before brace or equals
            if c == '{' or c == '=':
                new_line += "override "
            # Add extra padding before override if it's being before semicolon
            else: # ;
                new_line += " override"
            b_need_to_add_override = False

        if c == ')':
            b_reached_closed_paren = True

        new_line += c

    return new_line
    

# Takes in a the full path of a source file and the line number containing the
# CppCheck error. Rewrites the file and replaces the line with the updated line.
# Calls fixer function to write new line using error category
def fixCppCheckError(fullFilePath, lineNumber, errorCat):

    # Don't bother with unit test errors
    if "utest" in fullFilePath:
        return

    print("Fixing postfix error for line ", lineNumber, " in ", fullFilePath)

    tmp_file_name = 'new_file.tmp'

    with open(fullFilePath) as fp, open(tmp_file_name, 'w') as new_file:
        for i, line in enumerate(fp):
            curLine = i + 1 # CppCheck uses 1-based indexing with respect to lines
            if curLine == lineNumber:
                new_line = ''
                if errorCat == CppCheckError.POSTFIX_OPERATOR:
                    new_line = getPrefixFromPostfixLine(line)
                elif errorCat == CppCheckError.MISSING_OVERRIDE:
                    new_line = getOverrideLine(line)
                else:
                    print("Unhandled error category: ", errorCat)
                    return
                new_file.write(new_line)
            else:
                new_file.write(line)
    os.replace(tmp_file_name, fullFilePath)

