from enum import Enum

# Enum defining different types of CppCheck errors and detections
class CppCheckError(Enum):
    POSTFIX_OPERATOR = 1
    SPRINTF_OVERLAPPING_DATA = 2
    USE_INITIALIZATION_LIST = 3
    MISSING_OVERRIDE = 4
    NONE = 100
